$(function() {

  $('.js-chosen').chosen();

  // copy product image on 1st and 2nd section of aside from 3rd section
  // for responsive shizz
  $('.included-in-this-offer')
    .append($('#payment-page .product-image > img').clone());
  $('.your-price-only')
    .append($('#payment-page .product-image > img').clone());

  svgeezy.init(false, 'png');

  if(($('html').hasClass('ie7'))){
    $('*').each(function(){
      if($(this).css('display')=='block'){
        var f, a, n;
        f = $(this).outerWidth();
        a = $(this).width();
        n = a-(f-a);
        $(this).css('width', n);
      }
    });
  }

});
