$(function() {
  var $whatYouGet, 
      $enterEmail, 
      $paymentAside, 
      $paymentArticle,
      $aside,
      $article,
      $productImage,
      $productSection

  enquire
  .register("screen and (max-width: 800px)", {
    setup: function() {
      $productImage = $('#front-page .product img');
    },
    match: function() {
      $productImage.appendTo('#front-page > article:eq(0)');
    },
    unmatch: function() {
      $productImage.appendTo('#front-page .product');
      console.log($productImage);
    },
    deferSetup: true
  })
  .register("screen and (max-width: 600px)", {
    setup: function() {
      $paymentAside = $('#payment-page > .content-container > aside');
      $paymentArticle = $('#payment-page > .content-container > article');
      $enterEmail = $('#front-page .enter-email');
      $whatYouGet = $('#front-page .what-you-get');
      $aside = $('#front-page aside');
    },
    match: function() {
      $paymentAside.insertBefore($paymentArticle);
      $enterEmail.insertBefore($whatYouGet);
    },
    unmatch: function() {
      $paymentAside.insertAfter($paymentArticle);
      $aside.append($enterEmail);
    },
    deferSetup: true
  })
  .register("screen and (max-width: 400px)", {
    setup: function() {},
    match: function() {},
    unmatch: function() {},
    deferSetup: true
  });

});
