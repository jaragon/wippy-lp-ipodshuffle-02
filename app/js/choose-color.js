$(function() {
  // convert <select> to <ul>
  // attach onclick handler on to select proper element

  var $chooseColorSelect = $('.js-choose-color');
  var $productImage = $('.product-image');
  var chooseColor = [];
  $chooseColorSelect.children('option').each(function() {
    var elem = $('<a>', {
      'href': '#',
      'data-value': $(this).attr('value'),
      'data-img': $(this).attr('data-img'),
      'text': $(this).text()
    }).css('background-color', $(this).attr('data-color'))
      .wrap('<li></li>').parent();
    chooseColor.push(elem);
  }).parent().hide();

  //$('<ul>', { class: 'js-choose-color' }).append(chooseColor).insertAfter('.js-choose-color');
  $('<ul class="js-choose-color">').append(chooseColor).insertAfter('.js-choose-color');

  $chooseColorSelectA = $chooseColorSelect.next('ul').find('a');

  $chooseColorSelectA.click(function(e) {
    $chooseColorSelect.val($(this).attr('data-value'));
    $chooseColorSelectA.each(function() { $(this).parent().removeClass('selected'); });
    $(this).parent().addClass('selected');
    ako = $(this);
    $productImage.fadeTo(100, 0, function() {
      $productImage.attr('src', ako.attr('data-img'));
    }).fadeTo(100,1);
    e.preventDefault();
    return false;
  });


});
